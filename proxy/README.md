# Reverse Proxy

A reverse proxy is a type of proxy server that retrieves resources on behalf of a client from one or more servers. These
resources are then returned to the client, appearing as if they originated from the reverse proxy server itself.

In short, making sure that incoming request arrives at the right destination, and that the response back does the same.

## nginx

This project configures an instance of nginx.

### Generate Let's Encrypt certificates

The domains are registered with Njalla. By using their certbot plugin, the request can be fully automated.

```shell
certbot certonly -v --agree-tos -m webadmin@olof.info -a dns-njalla --dns-njalla-credentials njalla.ini --domains *.olof.info --domains olof.info
certbot certonly -v --agree-tos -m webadmin@olof.info -a dns-njalla --dns-njalla-credentials njalla.ini --domains mat-app.top --domains www.mat-app.top
certbot certonly -v --agree-tos -m webadmin@olof.info -a dns-njalla --dns-njalla-credentials njalla.ini --domains zusammen.in --domains www.zusammen.in
certbot certonly -v --agree-tos -m webadmin@olof.info -a dns-njalla --dns-njalla-credentials njalla.ini --domains tillsammans.in --domains www.tillsammans.in
certbot certificates
certbot plugins
```

#### Certificate permissions

`certbot` needs to run as root. `nginx` process does not, for security reasons.
For nginx to access the files they need to be owned by the same user/group.

```shell
➜  ~ docker run -it --rm nginxinc/nginx-unprivileged:1.26.0 bash
nginx@df7a3159083f:/$ id
uid=101(nginx) gid=101(nginx) groups=101(nginx)
```

The resulting certificate ownership thus needs to be adjusted:

```shell
chown -R 101:101 /etc/letsencrypt
```

### Default SSL route

nginx always need a certificate and a key to serve HTTPS traffic.

When configuring a "catch-all" address, used as a default server, the same requirement stands.
This certificate is basically a dummy, but still needed as else the startup check fails.

```shell
openssl req -x509 -nodes -newkey rsa:2048 -keyout default.key -out default.crt
```

## Firewall

The server running docker needs to accept incoming traffic. One way of setting this up is with `ufw`.
UFW or Unpretentious Firewall is an iptables front-end.

```shell
sudo apt install ufw -y
sudo systemctl enable ufw --now
systemctl status ufw
sudo ufw enable
sudo ufw status
```

To allow http, https and ssh traffic with ufw the following configuration can be used.

Note that as an additional safety measure, SSH access per key only (no password) can be setup by
setting `PasswordAuthentication no` in /etc/ssh/ssh_config and restarting the ssh daemon.

```shell
sudo ufw allow from 192.168.0.0/24 to any port 22 proto tcp
sudo ufw allow http
sudo ufw allow https
```

To verify the configuration, use `sudo ufw status`.

One way to check the ports is to use `sudo nc -zv -w 5 $IP_ADDRESS 22 80 443`. This is worth doing both with the
internal (192.168.X.Y), and the external IP (could be anything, for example 37.120.35.155).

## Netplan

Ubuntu server uses netplan to configure (physical) network interfaces.
Note that the key after ethernets / wifis depends on the hardware used.

Here is an example configuration `/etc/netplan/ethernet.yaml`:

```yaml
network:
  version: 2
  renderer: networkd
  ethernets:
    end0:
      dhcp4: true
      dhcp6: true
```

Here is an example configuration for wifi `/etc/netplan/wifi.yaml`:

```yaml
network:
  version: 2
  renderer: networkd
  wifis:
    wlp5s0:
      regulatory-domain: "DE"
      optional: true
      dhcp4: true
      access-points:
        "wifi-ssid":
          password: "wifi-password"
```

## VPN

This setup is using tailscale for a VPN tunnel.

The VPN tunnel is used to route incoming traffic from the proxy.

The docker hosts are using tailscale with subnet routers to route traffic. That way each service does not need
tailscale, but a single site-to-site link is re-used.

`tailscale up --advertise-routes=172.18.0.0/16 --snat-subnet-routes=false --stateful-filtering=false --accept-routes` (on one host)
`tailscale up --advertise-routes=172.31.0.0/16 --snat-subnet-routes=false --stateful-filtering=false --accept-routes` (on the other host)

Stateful filtering should be disabled if Docker is used; else this may prevent Docker containers from
connecting to Tailscale nodes. See https://tailscale.com/s/stateful-docker

## Docker networks

Each public facing server is connected to the same docker network ("public"), which per server need to use its own subnet.

As tailscale needs different subnets for the routing to work, we create two docker subnets:

`docker network create --subnet=172.18.0.0/16 --gateway=172.18.0.1 public`
`docker network create --subnet=172.31.0.0/16 --gateway=172.31.0.1 public`

## Debugging

List open local ports
`sudo nmap localhost`

List docker network information
`docker network inspect public`

Connect to IP address per simple HTTP on port 80
`curl -v 172.18.0.2`

List routes available
`ip route show`

## Resources

* https://letsdebug.net
* https://mindsers.blog/en/post/https-using-nginx-certbot-docker/
* https://www.nginx.com/blog/using-free-ssltls-certificates-from-lets-encrypt-with-nginx/
* https://www.nginx.com/blog/avoiding-top-10-nginx-configuration-mistakes/
* https://eff-certbot.readthedocs.io/en/latest/using.html
* https://tailscale.com/kb/1019/subnets
* https://tailscale.com/kb/1214/site-to-site
* https://docs.nginx.com/nginx/admin-guide/content-cache/content-caching/
* https://nginx.org/en/docs/http/request_processing.html
* https://blog.nginx.org/blog/nginx-caching-guide
* https://plausible.io/docs/proxy/guides/nginx
* https://nginx.viraptor.info/
* https://blog.nginx.org/blog/http-strict-transport-security-hsts-and-nginx
* https://nginx.org/en/docs/syntax.html
