server {
  listen 80;
  listen [::]:80;

  server_name mat-app.top www.mat-app.top;

  return 301 https://$host$request_uri;
}

proxy_cache_path /data/nginx/cache/mat_app keys_zone=mat_app_cache:10m use_temp_path=off;

upstream mat_app {
  server 172.31.0.4:80;
  zone mat_app_zone 32k;
  keepalive 2;
}

server {
  listen 443 ssl;
  listen [::]:443 ssl;
  http2 on;

  server_name mat-app.top www.mat-app.top;

  ssl_certificate /etc/nginx/ssl/live/mat-app.top/fullchain.pem;
  ssl_certificate_key /etc/nginx/ssl/live/mat-app.top/privkey.pem;

  location / {
    proxy_set_header Host $host;
    proxy_set_header X-Real-IP $remote_addr;

    add_header X-Cache-Status $upstream_cache_status;
    add_header X-Frame-Options "SAMEORIGIN";
    add_header X-Content-Type-Options "nosniff";
    add_header Strict-Transport-Security "max-age=63072000; includeSubDomains; preload";

    proxy_pass http://mat_app/;
    proxy_http_version 1.1;

    proxy_cache mat_app_cache;
    # cache HTTP 200 responses for 3 days
    proxy_cache_valid 200 3d;
    # cache HTTP 404 responses for 30 minutes
    proxy_cache_valid 404 30m;

    # return cached content if origin is is down
    proxy_cache_use_stale error timeout updating http_500 http_502 http_503 http_504;
    proxy_cache_background_update on;
  }
}
