# eclipse-mosquitto

Eclipse Mosquitto - An open source MQTT broker.

https://github.com/eclipse-mosquitto/mosquitto

Both MQTT and websockets are set up.

## Configuration

* The `mosquitto.conf` file is used for most config.
* The credentials are set in the `pwfile`. The file is configured with the `mosquitto_passwd` cli.

## File ownership

The credentials file needs to be owned by the `nobody` user: `chown 65534:65534 pwfile`.
It should also have restricted access permissions: `chmod 0700 pwfile`.

## Test

```shell
mosquitto_pub -t 'topic' -q 1 -p 1883 --pw password --username roger -m 'Hello from Mosquitto CLI'
```

There is a test mqtt.js client ready.

This can be used to test plain MQTT, Secure MQTT, WebSocket MQTT and Secure WebSocket MQTT connections.

```shell
docker compose run -it --rm mqtt-client bash
node@ab9c2407d605:~/mqttjs$ pnpm run
Commands available via "pnpm run":
  simple
    node src/simple.mjs
  secure
    node src/secure.mjs
  ws
    node src/ws.mjs
  wss
    node src/wss.mjs
node@ab9c2407d605:~/mqttjs$
```

## Resources

* [Configuration](https://mosquitto.org/man/mosquitto-conf-5.html)
