# certbot

This is the standard certbot docker image, with the added Njalla plugin.

Make sure to prepare a `njalla.ini` file, here an example:

```
dns_njalla_token=<token>
```

```shell
/opt/certbot # certbot plugins
Saving debug log to /var/log/letsencrypt/letsencrypt.log

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
* dns-njalla
Description: Obtain certificates using a DNS TXT record (if you are using Njalla
for DNS).
Interfaces: Authenticator, Plugin
Entry point: EntryPoint(name='dns-njalla',
value='certbot_dns_njalla.dns_njalla:Authenticator', group='certbot.plugins')

* standalone
Description: Runs an HTTP server locally which serves the necessary validation
files under the /.well-known/acme-challenge/ request path. Suitable if there is
no HTTP server already running. HTTP challenge only (wildcards not supported).
Interfaces: Authenticator, Plugin
Entry point: EntryPoint(name='standalone',
value='certbot._internal.plugins.standalone:Authenticator',
group='certbot.plugins')

* webroot
Description: Saves the necessary validation files to a
.well-known/acme-challenge/ directory within the nominated webroot path. A
seperate HTTP server must be running and serving files from the webroot path.
HTTP challenge only (wildcards not supported).
Interfaces: Authenticator, Plugin
Entry point: EntryPoint(name='webroot',
value='certbot._internal.plugins.webroot:Authenticator',
group='certbot.plugins')
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
/opt/certbot #
```
