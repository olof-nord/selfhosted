#!/bin/sh

set -e

/usr/local/bin/certbot renew --quiet -a dns-njalla --dns-njalla-credentials /opt/certbot/njalla.ini --cert-name olof.info
/usr/local/bin/certbot renew --quiet -a dns-njalla --dns-njalla-credentials /opt/certbot/njalla.ini --cert-name mat-app.top
/usr/local/bin/certbot renew --quiet -a dns-njalla --dns-njalla-credentials /opt/certbot/njalla.ini --cert-name zusammen.in
/usr/local/bin/certbot renew --quiet -a dns-njalla --dns-njalla-credentials /opt/certbot/njalla.ini --cert-name tillsammans.in
