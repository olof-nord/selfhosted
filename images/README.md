# images

This folder contains a Makefile to build and push the Docker images used by the other projects.

The images depend on git submodules.

Some local adjustments are needed as to cater for `risc-v` for now - see the Makefile prepare targets.

## Images

* certbot: `build-certbot-image`
* mariadb: `build-mariadb-image`
* memcached: `build-memcached-image`
* nginx: `build-nginx-image`
* postgres: `build-postgres-image`

See the [Makefile](./Makefile) for all details.

## Resources

* [failed running [/dev/.buildkit_qemu_emulator /bin/sh -c which ls]: exit code: 127](https://github.com/docker/buildx/issues/464)
