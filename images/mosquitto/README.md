# mosquitto

Eclipse Mosquitto is a message broker that implements the MQTT protocol.
MQTT provides a lightweight method of carrying out messaging using a publish/subscribe model.

## credentials file

mosquitto_passwd is a tool for managing password files for mosquitto.

```shell
mosquitto_passwd -c /etc/mosquitto/pwfile user
```
