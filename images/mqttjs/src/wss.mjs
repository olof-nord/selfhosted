import mqtt from "mqtt";

const options = {
  port: 443,
  // connect through the public nginx reverse proxy
  host: "mqtt.olof.info",
  protocol: "wss",
  username: process.env.USERNAME,
  password: process.env.PASSWORD,
};

const client = mqtt.connect(options);

client.on("connect", () => {
  console.log(`Connected using protocol ${options.protocol} to host ${options.host} on port ${options.port}`);
});
client.on("error", (error) => {
  console.error(error);
  client.end();
});

