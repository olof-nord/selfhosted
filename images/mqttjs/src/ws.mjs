import mqtt from "mqtt";

// this is based on the example here
// https://github.com/mqttjs/MQTT.js/blob/main/examples/ws/client.js

const options = {
  port: 9001,
  // connect through the docker internal network
  host: "mosquitto",
  protocol: "ws",
  username: process.env.USERNAME,
  password: process.env.PASSWORD,
};

const client = mqtt.connect(options);

client.on("connect", () => {
  console.log(`Connected using protocol ${options.protocol} to host ${options.host} on port ${options.port}`);
});
client.on("error", (error) => {
  console.error(error);
  client.end();
});

