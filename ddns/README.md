# Dynamic DNS (DDNS)

Internet service providers do not typically assign static public IP addresses to residential home users. You may find
one day that your cable modem/router was reset for some reason. After the modem/router came back online, it was
potentially assigned a new public IP address by your ISP's DHCP.

DDNS runs a minimal web application from within your home server that periodically sends an update of what IP address
you're currently using to your DNS provider. It authenticates to the DNS provider via a passkey(s) that is assigned by
the DNS provider, and if your IP address ever changes the DNS provider will update their A Records automatically.

## ddns-updater

This project configures an instance of the ddns-updater.

See their GitHub repository for provider-specific configuration options, for example
for [Njalla](https://github.com/qdm12/ddns-updater/blob/master/docs/njalla.md).

In the same GitHub repository, an [example](https://github.com/qdm12/ddns-updater/blob/master/docker-compose.yml)
docker-compose file is provided.

The configuration is provided by a JSON config file, see the example file for a start.
Rename it to config.json and copy it to `data/config.json`.
