# ihatemoney

A simple shared budget manager web application.

https://github.com/spiral-project/ihatemoney

## Configuration

A `.env` file needs to be added with configuration for the postgres database as well as the ihatemoney project itself.

## Resources

* [Configuration](https://ihatemoney.readthedocs.io/en/latest/configuration.html)
