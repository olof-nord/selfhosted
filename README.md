# selfhosted

My self-hosted applications.

## Projects

### Dynamic DNS (DDNS)

This keeps my DNS names up to date. See [ddns](./ddns).

### Proxy

This is a nginx reverse proxy which routes incoming requests. See [proxy](./proxy).

### Seafile

Seafile is an open source cloud storage system with file encryption and group sharing. See [seafile](./seafile).

### Commento++

Commento++ is a free, open source, fast & lightweight comments box that you can embed in your static website instead of
Disqus. See [commento](./commento).
