# commento

This project uses [commentoplusplus](https://github.com/souramoo/commentoplusplus), a fork of the commento project.

## Commento riscv64

Commento is built using the commentoplusplus Dockerfile, with the addition of the below envs to the backend `api-build`
step for riscv64 cross-compiling.

```dockerfile
# cross-compile
ENV GOARCH=riscv64
ENV GOOS=linux
```

## Postgres

There are no official riscv64 postgres images, but we can build them from source.

For this to work, adjust the [Dockerfile](postgres/14/bullseye/Dockerfile)

```dockerfile
FROM riscv64/debian:unstable-slim
```

## Resources

* [Configuring the backend](https://docs.commento.io/configuration/backend/)
