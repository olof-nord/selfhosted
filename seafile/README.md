# File Sync and File Share

Seafile is an open source cloud storage system with file encryption and group sharing.

https://www.seafile.com/

## seafile

This project uses a docker image kindly provided
by [ChatDeBlofeld](https://github.com/ChatDeBlofeld/seafile-arm-docker-base), available for both arm64 and riscv64.

### configuration

Note that the instance needs to be restarted for any changes to take effect.

Seafile 9 comes with a new file-server written in golang, which should provide higher performance, however it is
disabled per default. It can be enabled by adding the following configuration.

/shared/conf/seafile.conf
```toml
[fileserver]
use_go_fileserver = true
```

The seafile instance supports memcached, but it is not enabled by default. It can be enabled by adding the following
configuration.

/shared/conf/seahub_settings.py
```python
CACHES = {
  'default': {
    'BACKEND': 'django_pylibmc.memcached.PyLibMCCache',
    'LOCATION': 'memcached:11211',
  },
}
```

See [official documentation](https://manual.seafile.com/config/) for all configuration options.

### logs

See logs:
```shell
docker-compose exec seafile tail -f /shared/logs/controller.log
docker-compose exec seafile tail -f /shared/logs/fileserver.log
docker-compose exec seafile tail -f /shared/logs/seafile.log
docker-compose exec seafile tail -f /shared/logs/seahub.log
```

## mariadb

Database used by Seafile. Note that the initialization of the DB should only set the root user
password, as the seafile initialization scripts adds and uses a `seafile` user during startup.

As there is not yet any official riscv64 docker images available, this project uses a mix of the official mariadb-docker
repository Dockerfile and entrypoint script together with the mariadb package provided by Ubuntu.

## memcached

Memory caching for speeding up database lookups.

Get stats for the running instance:

```shell
docker-compose exec memcached /src/scripts/memcached-tool localhost:11211 stats
```
